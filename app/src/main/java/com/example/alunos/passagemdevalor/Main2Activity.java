package com.example.alunos.passagemdevalor;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {
    TextView texto;
    Button btnVoltar;
    String valFromAct1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        texto = (TextView)findViewById(R.id.textView);
        btnVoltar = (Button)findViewById(R.id.button2);
        valFromAct1 = getIntent().getExtras().getString("Value");
        texto.setText(valFromAct1);
    }
    public void btnVoltar(View v){
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
        finish();
    }
}
