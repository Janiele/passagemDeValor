package com.example.alunos.passagemdevalor;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    EditText texto;
    Button btnProx;
    String editTextValor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        texto= (EditText)findViewById(R.id.editText);
        btnProx = (Button)findViewById(R.id.button);

    }
    public void btnProx(View v){
        Intent i = new Intent(this, Main2Activity.class);
        editTextValor = texto.getText().toString();
        i.putExtra("Value", editTextValor);
        startActivity(i);
        finish();

    }
}
